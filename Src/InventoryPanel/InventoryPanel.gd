extends CanvasLayer

var sandbox = null
var player = null

func _ready():
	assert(self.sandbox)
	assert(self.player)
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	
	for item in self.player.inventory.items:
		var label = Label.new()
		label.text = item.name
		label.modulate = Color.black
		label.theme = preload("res://Themes/futur.theme")
		$VBoxContainer/List.add_child(label)
	
func _process(delta):
	if Input.is_action_just_pressed("show_inventory"):
		self.quit()
		
func quit():
	$VBoxContainer/List.get_children().clear()
	get_tree().paused = false
	self.sandbox.close_inventory()
	self.queue_free()
