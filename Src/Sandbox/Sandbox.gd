extends Node2D

var log_book_open : bool = false
var inventory_open : bool = false
var map_name = 'oriniel'

func _ready():
	GItems.player = $Map/YSort/Actor
	GSave.init($Map/YSort/Actor)
	GLoad.init($Map/YSort/Actor)
	GSave.sandbox = self
	GLoad.sandbox = self
	
	$Map/YSort/Actor.state.connect('on_speak_to', self, '_on_speak_to')
	$Map/YSort/Actor.state.connect('on_battle', self, '_on_battle')

	if GLoad.new_game:
		GLoad.reset()
	else:
		GLoad.load_game()
	
	GQuest.connect("quest_started", self, '_on_quest_started')
	GQuest.connect("quest_finished", self, '_on_quest_finished')

func _process(delta):
	if $Map != null and not $Map/YSort/Actor.state.is_connected('on_speak_to', self, '_on_speak_to'):
		$Map/YSort/Actor.state.connect('on_battle', self, '_on_battle')
		$Map/YSort/Actor.state.connect('on_speak_to', self, '_on_speak_to')
	
	
	if Input.is_action_just_pressed("show_log_book"):
		show_log_book()
	
	if Input.is_action_just_pressed("show_inventory"):
		show_inventory()
		
	if Input.is_action_just_pressed("pause"):
		var p = preload("res://PausePanel/PausePanel.tscn").instance()
		self.add_child(p)
	
	if $Map != null:
		$Camera2D.smoothing_enabled = true
		$Camera2D.position = $Map/YSort/Actor.position
	else: 
		$Camera2D.smoothing_enabled = false
		$Camera2D.position =+ $Camera2D.get_viewport_rect().size * 0.5

func show_log_book():	
	if not self.log_book_open:
		var log_book = preload("res://LogBook/LogBook.tscn").instance()
		log_book.sandbox = self
		self.log_book_open = true
		add_child(log_book)

func show_inventory():	
	if not self.inventory_open:
		var inv = preload("res://InventoryPanel/InventoryPanel.tscn").instance()
		inv.sandbox = self
		inv.player = $Map/YSort/Actor
		self.inventory_open = true
		add_child(inv)
		
func close_inventory():
	self.inventory_open = false
		
func close_log_book():
	self.log_book_open = false
	
func _on_quest_started(quest):
	var quest_panel = preload("res://QuestPanel/QuestPanel.tscn").instance()
	quest_panel.started = true
	quest_panel.quest = quest
	add_child(quest_panel)
	
func _on_quest_finished(quest):
	var quest_panel = preload("res://QuestPanel/QuestPanel.tscn").instance()
	quest_panel.started = false
	quest_panel.quest = quest
	add_child(quest_panel)
	
func _on_speak_to(npc):
	var dial_panel = preload("res://DialogPanel/DialogPanel.tscn").instance()
	var dial = GDialog.get_dialog(npc)
	
	if dial != null:
		dial_panel.sandbox = self
		dial_panel.dial = dial
		dial_panel.npc = npc
		var dial_pos = npc.position
		
		dial_panel.set_position(dial_pos)
		self.add_child(dial_panel)

func _on_battle(enemy):
	var battle_panel = preload("res://BattlePanel/BattlePanel.tscn").instance()
	battle_panel.enemy = enemy
	add_child(battle_panel)

func move(location, message = ''):
	if location == 'oriniel':
		var n = preload("res://OrinielMap/OrinielMap.tscn").instance()
		n.name = 'Map'
		self.map_name = 'oriniel'
		var transition = preload("res://Transition/Transition.tscn").instance()
		transition.sandbox = self
		transition.next = n
		if message == '':
			transition.text = 'Village d\'Oriniel'
		else:
			transition.text = message
		
		add_child(transition)
		var inv = GItems.player.inventory
		remove_child($Map)
		
		GItems.player = n.get_node("YSort/Actor")

		GItems.player.inventory = inv
		
	elif location == 'desert':
		var n = preload("res://DesertMap/DesertMap.tscn").instance()
		n.name = 'Map'
		self.map_name = 'desert'
		var transition = preload("res://Transition/Transition.tscn").instance()
		transition.sandbox = self
		transition.next = n
		if message == '':
			transition.text = 'Grand Desert'
		else:
			transition.text = message
		add_child(transition)
		var inv = GItems.player.inventory
		remove_child($Map)
		
		GItems.player = n.get_node("YSort/Actor")
		GItems.player.inventory = inv
	else:
		print(location)
		assert(false)
	
