extends Control

var sandbox
var dial = null
var npc = null
var processed_dial = []

func _ready():
	$ScreenBackground/RichTextLabel.text = self.dial.text

	self.pause_mode = Node.PAUSE_MODE_PROCESS
	GPause.pause(self)
	
	GQuestTracker.last_npc_dialog = self.npc
	
func _process(delta):
	$BtnHint.visible = (Input.get_connected_joypads().size() > 0)
	$ScreenBackground.rect_min_size.y = $ScreenBackground/RichTextLabel.rect_size.y
	$BtnHint.position = $ScreenBackground.rect_position
	$BtnHint.position.y += $ScreenBackground.rect_size.y
	
	if Input.is_action_just_pressed("ui_cancel") and self.dial.next == null:
		self.quit()
	
	if Input.is_action_just_pressed("ui_accept"):
			
		if self.dial.next != null:
			var n = GDialog.get_by_id(self.dial.next)
			if n:
				self.process_next_dial()
				self.dial = n
				$ScreenBackground/RichTextLabel.text = self.dial.text
			else:
				quit()
		else:
			quit()

func process_next_dial():
	if self.dial.id in self.processed_dial:
		return
	
	self.processed_dial.push_back(self.dial.id)
	
	for item_name in self.dial.item_rewards:
		GItems.give_player(item_name)
		
	for item_name in self.dial.item_remove:
		var inv = GItems.player_inventory()
		inv.remove(item_name)
		
	assert(self.sandbox)
	if self.dial.move != null:
		self.sandbox.move(self.dial.move[0], self.dial.move[1])
		
	
	
func quit():
	assert(self.npc != null)	
	self.process_next_dial()

	if self.dial.next_quest != null:
		GQuest.start_quest(self.dial.next_quest)
		
	GQuestTracker.last_npc_dialog = null
	GPause.unpause(self)
	self.queue_free()
	
