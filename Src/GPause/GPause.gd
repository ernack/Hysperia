extends Node

var queue = []

func _ready():
	self.pause_mode = Node.PAUSE_MODE_PROCESS

func pause(from):
	get_tree().paused = true
	self.queue.push_back(from)
	
func unpause(from):
	self.queue.erase(from)
	
	if self.queue.empty():
		get_tree().paused = false
