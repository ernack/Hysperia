extends Node

var super = null
var animation_player = null
var sprite = null
var nearest_npc = null

signal on_speak_to
signal on_battle

func _ready():
	self.animation_player = get_node('../../AnimationPlayer')
	self.sprite = get_node('../../Sprite')

func update(delta):
	var dir = Vector2(0, 0)
	
	if Input.is_action_pressed('player_up'):
		self.animation_player.play("walk_up")
		dir.y = -1
		
	elif Input.is_action_pressed('player_down'):
		self.animation_player.play("walk_down")
		dir.y += 1
	
	elif Input.is_action_pressed('player_left'):
		self.sprite.flip_h = false
		self.animation_player.play("walk_side")
		dir.x -= 1

	elif Input.is_action_pressed('player_right'):
		self.animation_player.play("walk_side")
		self.sprite.flip_h = true
		dir.x += 1
	
	if Input.is_action_just_released('player_down'):
		self.animation_player.stop()
		
	if Input.is_action_just_released('player_left'):
		self.animation_player.stop()
		
	if Input.is_action_just_released('player_up'):
		self.animation_player.stop()
		
	if Input.is_action_just_released('player_right'):
		self.animation_player.stop()
	
	if Input.is_action_just_pressed("ui_accept"):
		
		if self.nearest_npc != null:
			if self.nearest_npc.is_in_group('enemy'):
				emit_signal("on_battle", self.nearest_npc)
			elif self.nearest_npc.is_in_group('NPC') and GQuestTracker.last_npc_dialog == null:
				emit_signal("on_speak_to", self.nearest_npc)
				
		
	update_nearest_npc()
	if dir.length_squared() != 0:
		dir = dir.normalized()
		super.velocity = dir * super.speed * delta
		super.move_and_collide(super.velocity)
		GQuestTracker.player_pos = super.position

func update_nearest_npc():
	var dist = null
	var nearest_npc = null
	self.nearest_npc = null
	
	for npc in get_tree().get_nodes_in_group('NPC'):
		npc.modulate = Color(0.8, 0.8, 0.8)
		
	for npc in get_tree().get_nodes_in_group('NPC'):
		var my_dist = (super.position - npc.position).length()
		
		if dist == null or my_dist < dist:
			dist = my_dist
			nearest_npc = npc
	
	if dist and dist <= GDialog.MIN_DISTANCE:
		self.nearest_npc = nearest_npc
		if self.nearest_npc != null:
			self.nearest_npc.modulate = Color(1, 1, 1, 1)
	
