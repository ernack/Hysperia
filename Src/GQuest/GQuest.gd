extends Node

signal quest_started
signal quest_finished

class Quest:
	var id : int
	var title : String
	var desc : String
	var only_one_time : bool
	var type : String
	
	func _init(title, desc):
		self.title = title
		self.desc = desc
		self.only_one_time = true

	func is_finished():
		return false

class GotoQuest extends Quest:
	var position : Vector2
	var min_dist : float = 16.0
	
	func _init(title, desc).(title, desc):
		self.position = Vector2(0, 0)
	
	func is_finished():
		return (self.position - GQuestTracker.player_pos).length() < self.min_dist

class SpeakQuest extends Quest:
	var npc_name : String
	var min_dist : float = 16.0

	func _init(title, desc, npc_name).(title, desc):
		self.npc_name = npc_name

	func is_finished():
		var last = GQuestTracker.last_npc_dialog
		return last != null and last.actor_name == npc_name
		
class OwnQuest extends Quest:
	var items
	
	func _init(title, desc, items).(title, desc):
		self.items = items
		
	func is_finished():
		var inv = GItems.player_inventory()
		
		for item in self.items:
			if not(inv.has(GItems.get_item_by_id(item))):
				return false
		return true
		
class BattleQuest extends Quest:
	var target_name
	
	func _init(title, desc, target_name).(title, desc):
		self.target_name = target_name
		
	func is_finished():
		var last = GQuestTracker.last_battle_enemy
		return last != null and last == self.target_name
		
var all_quests = []
var open_quests = []
var close_quests = []

var current = null

func _ready():
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	var file : File = File.new()
	var err = file.open('res://GQuest/quests.json', file.READ)
	assert(err == OK)
	var dict = JSON.parse(file.get_as_text()).result
	
	for quest in dict['quests']:
		if quest['type'] == 'goto':
			var new_quest = GotoQuest.new(quest['title'], quest['desc'])
			new_quest.id = quest['id']
			var pos = Vector2(float(quest['pos_x']), float(quest['pos_y']))
			new_quest.position = pos
			new_quest.type = 'goto'
			self.all_quests.push_back(new_quest)
			
		if quest['type'] == 'speak':
			var new_quest = SpeakQuest.new(quest['title'], quest['desc'], quest['npc_name'])
			new_quest.id = quest['id']
			new_quest.type = 'speak'
			self.all_quests.push_back(new_quest)
			
		if quest['type'] == 'own_item':
			var new_quest = OwnQuest.new(quest['title'], quest['desc'], quest['item_owned'])
			new_quest.id = quest['id']
			new_quest.type = 'own_item'
			self.all_quests.push_back(new_quest)
		
		if quest['type'] == 'battle':
			var new_quest = BattleQuest.new(quest['title'], quest['desc'], quest['target'])
			new_quest.id = quest['id']
			new_quest.type = 'battle'
			self.all_quests.push_back(new_quest)
			
		
	
func get_current():
	if self.current == null or self.current >= self.open_quests.size():
		return null
	return self.open_quests[self.current]
	
func get_quest(id):
	for quest in self.all_quests:
		if quest.id == id:
			return quest
	return null
	
func _process(delta):
	for quest in self.open_quests:
		if quest != null and quest.is_finished():
			self.emit_signal("quest_finished", quest)
			self.open_quests.erase(quest)
			self.close_quests.push_back(quest)
			self.current = null
		
func start_quest(quest):
	for q in self.open_quests:
		if quest.title == q.title:
			return
	if quest.only_one_time and quest in self.close_quests:
		return
		
	self.open_quests.push_back(quest)
	self.current = self.open_quests.size() - 1
	emit_signal("quest_started", quest)
