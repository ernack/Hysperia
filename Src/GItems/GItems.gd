extends Node
var player = null
var items : Dictionary

class Item:
	var name
	var id
	var atk
	
	func _init(name):
		self.name = name
		self.id = -1
		self.atk = 1
		
func _ready():
	var file : File = File.new()
	var err = file.open('res://GItems/items.json', file.READ)
	assert(err == OK)
	var dict = JSON.parse(file.get_as_text()).result
	
	for item in dict['items']:
		self.items[item['name']] = Item.new(item['name'])
		self.items[item['name']].id = item['id']
		if item.has('atk'):
			self.items[item['name']].atk = item['atk']
		
func get_item_by_id(id):
	for item in self.items.values():
		if item.id == id:
			return item
	return null

func give_player(item):
	assert(self.player)
	if typeof(item) == TYPE_STRING:
		self.player.inventory.add(self.items[item])
	else:
		self.player.inventory.add(item)
		
func player_inventory():
	assert(self.player)
	return self.player.inventory
