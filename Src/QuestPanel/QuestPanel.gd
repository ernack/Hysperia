extends CanvasLayer

var quest = null
var started : bool = false

func _ready():
	if self.started:
		$Panel/VBoxContainer/Title.text = 'NOUVELLE QUÊTE :' + self.quest.title
	else:
		$Panel/VBoxContainer/Title.text = 'FIN DE QUÊTE :' + self.quest.title
		
	$Panel/VBoxContainer/Desc.text = self.quest.desc
	$Panel.modulate = Color(1, 1, 1, 1)
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	GPause.pause(self)

func _process(delta):
	var height = $Panel/VBoxContainer/Title.rect_size.y + $Panel/VBoxContainer/Desc.rect_size.y
	$Panel/ScreenBackground.rect_size.y = height
	$Panel.rect_size.y = height
	if not $Timer.is_stopped():
		$Panel.modulate = Color(1, 1, 1, sin($Timer.time_left/$Timer.wait_time))
		
	if Input.is_action_just_pressed("ui_cancel") or Input.is_action_just_pressed("ui_accept"):
		$Timer.stop()
		$BeforeTimer.stop()
		self.quit()
	
func _on_Timer_timeout():
	self.quit()

func quit():
	GPause.unpause(self)
	self.queue_free()

func _on_BeforeTimer_timeout():
	$Timer.start()
	$BeforeTimer.stop()
