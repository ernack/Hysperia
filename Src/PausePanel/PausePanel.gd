extends CanvasLayer

func _ready():
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	$Panel/VBoxContainer/Play.grab_focus()
	
func return_game():
	get_tree().paused = false
	self.queue_free()
	
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		self.return_game()
	
	if Input.is_action_just_pressed("pause"):
		self.return_game()
	
func _on_Play_button_down():
	self.return_game()

func _on_Quit_button_down():
	get_tree().quit(0)

func _on_Save_button_down():
	var dial = ConfirmationDialog.new()
	dial.dialog_text = 'Voulez vous vraiment sauvegarder la partie ?'
	
	self.add_child(dial)
	dial.connect("confirmed", self, '_on_save')
	dial.popup_centered()

func notify(text):
	var dial = AcceptDialog.new()
	dial.dialog_text = text
	self.add_child(dial)
	dial.popup_centered()
	
func _on_save():
	GSave.save_game()
	self.notify('Sauvegardé !')

func _on_Load_button_down():
	GLoad.load_game()
	self.notify('Chargé !')


func _on_BackMainMenu_button_down():
	get_tree().paused = false
	get_tree().change_scene("res://MainMenu/MainMenu.tscn")
