extends Control

var sandbox = null
var next = null
var text = ''

func _ready():
	assert(self.sandbox)
	assert(self.next)
	$Label.text = self.text

func _on_Timer_timeout():
	self.sandbox.add_child(self.next)
	self.queue_free()
	
