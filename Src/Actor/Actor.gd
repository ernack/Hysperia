extends KinematicBody2D

var speed = 512
var velocity : Vector2 = Vector2(0, 0)
export var actor_name : String
var inventory = null
var state = null
enum kind {PLAYER, NPC, ENEMY}
export(kind) var my_kind = kind.NPC
export var rect_pos : Vector2 = Vector2(0, 0)
 
func _ready():
	self.inventory = $Inventory
	if self.my_kind == kind.PLAYER:
		init_player()
	if self.my_kind == kind.NPC:
		init_npc()
	if self.my_kind == kind.ENEMY:
		init_enemy()	
		
func init_player():
	state = $Kind/Player
	state.super = self
	self.actor_name = 'player'
	
func init_npc():
	apply_texture_rect()
	$Name.text = self.actor_name
	state = $Kind/NPC
	state.super = self
	add_to_group('NPC')

func init_enemy():
	apply_texture_rect()
	$Name.text = name
	self.state = $Kind/Enemy
	self.state.super = self
	add_to_group('enemy')
	add_to_group('NPC')

func apply_texture_rect():
	$Sprite.texture = preload("res://Assets/Characters/characters.png")
	$Sprite.region_enabled = true
	$Sprite.region_rect = Rect2(16 * self.rect_pos, Vector2(16, 16))
		
func _process(delta):
	if self.state != null:
		state.update(delta)
