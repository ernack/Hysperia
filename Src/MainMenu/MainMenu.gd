extends Control


func _ready():
	$VBoxContainer/NewGame.grab_focus()

func preload_scene(new_game ):
	GLoad.new_game = new_game
	get_tree().change_scene("res://Sandbox/Sandbox.tscn")
	
func _on_NewGame_button_down():
	self.preload_scene(true)
	

func _on_LoadGame_button_down():
	self.preload_scene(false)


func _on_Quit_button_down():
	get_tree().quit(0)
