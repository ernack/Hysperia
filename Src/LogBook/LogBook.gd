extends CanvasLayer

var sandbox = null

func _ready():
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	$VBoxContainer/Back.grab_focus()
	
	for quest in GQuest.open_quests:
		if quest == null:
			continue
			
		var hbox = HBoxContainer.new()

		var title = Label.new()
		title.text = '[' + quest.title + ']        '
		title.theme = preload("res://Themes/futur.theme")
		title.modulate = Color(0, 0, 0)
		title.align = Label.ALIGN_LEFT
		
		var desc = Label.new()
		desc.align = Label.ALIGN_RIGHT
		desc.text = quest.desc
		desc.theme = preload("res://Themes/futur.theme")
		desc.modulate = Color(0, 0, 0)
		
		hbox.add_child(title)
		hbox.add_child(desc)
		$VBoxContainer/Quests.add_child(hbox)
		
func _process(delta):
	if Input.is_action_just_pressed("show_log_book"):
		self.quit()
	
func _on_Back_button_down():
	quit()
	
func quit():
	self.sandbox.close_log_book()
	get_tree().paused = false
	self.queue_free()

