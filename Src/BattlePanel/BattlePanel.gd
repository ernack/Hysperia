extends CanvasLayer

var enemy = null
var player_turn : bool = true

func _ready():
	assert(self.enemy != null)
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	$Panel/VBoxContainer/Name.text = self.enemy.actor_name
	
	$Panel/VBoxContainer/HBoxContainer/PlayerLife.rect_min_size.x = $Panel.rect_size.x/2
	$Panel/VBoxContainer/HBoxContainer/EnemyLife.rect_min_size.x = $Panel.rect_size.x/2
	
	var inv = GItems.player_inventory()
	for item in inv.items:
		var btn = Button.new()
		btn.text = 'Attaquer avec ' + item.name
		btn.connect("button_down", self, '_on_attack', [item])
		$Panel/VBoxActions.add_child(btn)
	if $Panel/VBoxActions.get_child_count() > 0:
		$Panel/VBoxActions.get_children()[0].grab_focus()

func disable_buttons(disabled):
	for btn in $Panel/VBoxActions.get_children():
		btn.disabled = disabled

func hurt_player(damages):
	$Panel/VBoxContainer/HBoxContainer/PlayerLife.value -= damages	
	if $Panel/VBoxContainer/HBoxContainer/PlayerLife.value <= 0:
		self.end(false)
		
func hurt_enemy(damages):
	$Panel/VBoxContainer/HBoxContainer/EnemyLife.value -= damages	
	if $Panel/VBoxContainer/HBoxContainer/EnemyLife.value <= 0:
		self.end(true)
		
func _on_attack(item):
	self.hurt_enemy(item.atk)
	self.player_turn = false
	self.disable_buttons(true)
	$EnemyResponseTimer.start()
	
func enemy_attack():
	self.hurt_player(rand_range(0, 25))
	self.player_turn = true
	self.disable_buttons(false)
	
func _on_EnemyResponseTimer_timeout():
	self.enemy_attack()

func end(won):
	get_tree().paused = false
	GQuestTracker.last_battle_enemy = self.enemy.actor_name
	if won:
		self.enemy.queue_free()
	else:
		get_tree().change_scene('res://FailMenu/FailMenu.tscn')
	
	self.queue_free()
