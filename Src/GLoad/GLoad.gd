extends Node

var player = null
var new_game : bool = false
var sandbox = null
func _ready():
	pass

func init(player):
	self.player = player

func reset():
	self.player.inventory.items.clear()
	GQuest.all_quests.clear()
	GQuest.open_quests.clear()
	GQuest.close_quests.clear()
	GQuestTracker.player_pos = Vector2(-INF, INF)
	GQuestTracker.last_npc_dialog = null
	GQuestTracker.last_npc_no_dialog_box = null
	GQuestTracker.last_battle_enemy = null
	self.new_game = true
	
func load_game():
	assert(self.player != null)
	var file : File = File.new()
	var err = file.open(GSave.SAVE_FILE_NAME, File.READ)
	if err != OK:
		return
	
	var line = file.get_line()

	var save_table = JSON.parse(line).result
	restore_map(save_table)
	restore_player(save_table)
	restore_npcs(save_table)
	restore_quests(save_table)
	restore_inventory(save_table)
	
func restore_map(save_table):
	self.sandbox.move(save_table['map'])

func restore_player(save_table):
	GItems.player.position.x = float(save_table['player_position_x'])
	GItems.player.position.y = float(save_table['player_position_y'])
	
func restore_npcs(save_table):
	assert(self.sandbox != null)
	var actors_list = null
	
	for c in self.sandbox.get_children():
		if c.name == 'Map':
			actors_list = c.get_children()[1]	
		
	if actors_list == null:
		return
		
	var alives = save_table['npcs']
	for actor in actors_list.get_children():
		if actor.actor_name != '' and not (actor.actor_name in alives):
			actors_list.remove_child(actor)
	
func restore_quests(save_table):
	var open = save_table['open_quests']
	for q in open:
		GQuest.open_quests.push_back(GQuest.get_quest(q))
		
	var close = save_table['close_quests']
	for q in close:
		GQuest.close_quests.push_back(GQuest.get_quest(q))

func restore_inventory(save_table):
	var items = save_table['inventory']
	for item_name in items:
		GItems.give_player(item_name)
		
