extends Node

const SAVE_FILE_NAME = 'user://savegame.json'
var player = null
var sandbox = null

func _ready():
	pass

func init(player):
	self.player = player
	
func save_game():
	assert(self.player != null)
	
	var save_table : Dictionary = {}
	
	self.collect(save_table)
	self.write(save_table)
	
func collect(save_table):
	self.collect_player(save_table)
	self.collect_npc(save_table)
	self.collect_quests(save_table)
	self.collect_inventory(save_table)
	self.collect_map(save_table)
	
func collect_player(save_table):
	save_table['player_position_x'] = GItems.player.position.x
	save_table['player_position_y'] = GItems.player.position.y

func collect_npc(save_table):
	assert(self.sandbox != null)	
	var alives = []
	var actors_list = self.sandbox.get_node('Map/YSort')
	for actor in actors_list.get_children():
		alives.push_back(actor.actor_name)
	save_table['npcs'] = alives
	
func collect_quests(save_table):
	var open = []
	var close = []
	
	for q in GQuest.open_quests:
		open.push_back(q.id)
	
	for q in GQuest.close_quests:
		close.push_back(q.id)
		
	save_table['open_quests'] = open
	save_table['close_quests'] = close
	
func collect_inventory(save_table):
	save_table['inventory'] = []
	
	for item in self.player.inventory.items:
		save_table['inventory'].append(item.name)
	
func collect_map(save_tables):
	save_tables['map'] = self.sandbox.map_name
	
func write(save_table):
	var file = File.new()
	file.open(self.SAVE_FILE_NAME, File.WRITE)
	file.store_line(to_json(save_table))
