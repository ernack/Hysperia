extends Node

var items : Array

func _ready():
	pass
	
func get_size():
	return items.size()

func add(item):
	self.items.push_back(item)
	
func remove(item_name):
	if typeof(item_name) != TYPE_STRING:
		item_name = item_name.name
		
	for item in self.items:
		if item.name == item_name:
			self.items.erase(item)
			return

func has(item):
	for it in self.items:
		if it.id == item.id:
			return true
	return false
