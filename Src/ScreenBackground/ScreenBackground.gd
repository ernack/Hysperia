extends ColorRect


func _ready():
	self.modulate = Color(0.5, 0.5, rand_range(0.9, 1.0), rand_range(0.5, 0.6))


func _on_Timer_timeout():
	self.modulate = Color(0.5, 0.5, rand_range(0.9, 1.0), rand_range(0.5, 0.6))

