extends Node

const MIN_DISTANCE = 128.0
var dialogs : Array

class Dialog:
	var id
	var text
	var next
	var quest
	var next_quest
	var actor_name
	var closed_quests
	var item_rewards
	var item_remove
	var item_owned
	var move
	
	func _init(text='[AUCUN TEXTE]', next = null, next_quest = null):
		self.id = -1
		self.text = text
		self.next = next
		self.quest = null
		self.next_quest = next_quest
		self.actor_name = null
		self.closed_quests = []
		self.item_rewards = []
		self.item_remove = []
		self.item_owned = []
		self.move = null
		
	func constraints_count():
		var count = 0
		count += self.closed_quests.size()
		count += self.item_owned.size()
		if self.quest != null:
			count += 1
		return count
		
func _ready():
	var file : File = File.new()
	var err = file.open('res://GDialog/dialogs.json', file.READ)
	assert(err == OK)
	var text = file.get_as_text()

	var parse_result = JSON.parse(text)
	var dialogs = parse_result.result
	
	for dialog in dialogs['dialogs']:
		var next_quest = null
		var quest = null
		var item_rewards = []
		var item_remove = []
		var move = null
		var next = null
		
		if dialog.has('item_rewards'):
			item_rewards = []
			for item_id in dialog['item_rewards']:
				var item = GItems.get_item_by_id(item_id)
				item_rewards.push_back(item)
				
		if dialog.has('item_remove'):
			item_remove = []
			for item_id in dialog['item_remove']:
				var item = GItems.get_item_by_id(item_id)
				item_remove.push_back(item)
		
		if dialog.has('next_quest') and dialog['next_quest'] != null:
			next_quest = GQuest.get_quest(int(dialog['next_quest']))
		
		if dialog.has('quest') and dialog['quest'] != null:
			quest = GQuest.get_quest(int(dialog['quest']))
			
		if dialog.has('next') and dialog['next'] != null:
			next = dialog['next']
			
		if dialog.has('move') and dialog['move'] != null:
			move = dialog['move']
		
		var new_dialog = Dialog.new(dialog['text'], null, next_quest)
		new_dialog.id = dialog['id']
		new_dialog.quest = quest
		new_dialog.actor_name = dialog['actor']
		new_dialog.item_rewards = item_rewards
		new_dialog.item_remove = item_remove
		new_dialog.move = move
		new_dialog.next = next
		
		if dialog.has('closed_quests'):
			for q in dialog['closed_quests']:
				var closed_quest = GQuest.get_quest(q)
				new_dialog.closed_quests.push_back(closed_quest)
				
		if dialog.has('item_owned'):
			for id in dialog['item_owned']:
				new_dialog.item_owned.push_back(GItems.get_item_by_id(id))

		self.dialogs.push_back(new_dialog)
	
func get_contextual_dialog(actor):
	var closed_id = []
	for closed in GQuest.close_quests:
		closed_id.push_back(closed.id)
	
	for dialog in self.dialogs:
		if dialog.actor_name != actor.actor_name:
			continue
		
		var found = true
		
		for closed in dialog.closed_quests:
			if not(closed.id in closed_id):
				found = false
				break
		
		if found and not dialog.closed_quests.empty():
			return dialog
		
	return null

func get_inventory_dialog(actor):
	var inv = GItems.player_inventory()
	
	for dialog in self.dialogs:
		if dialog.actor_name != actor.actor_name or dialog.item_owned.empty():
			continue
		
		for item in dialog.item_owned:
			if not inv.has(item):
				return null
		
		return dialog
	return null
 
func prev(dial):
	for d in self.dialogs:
		if d != null and d.next != null and d.next == dial.id:
			return d
	return null
	
func get_dialog(actor):
	var all = []
	for dial in self.dialogs:
		if dial.actor_name != actor.actor_name:
			continue

		var current_quest = GQuest.get_current()
		
		if dial.quest != null and (current_quest == null or current_quest.id != dial.quest.id):
			continue
			
		var err = false
		for closed_quest in dial.closed_quests:
			if not(closed_quest in GQuest.close_quests):
				err = true
				break
		if err:
			continue
		
		err = false
		for item in dial.item_owned:
			if not(item in GItems.player_inventory().items):
				err = true
				break
		if err:
			continue
		all.append(dial)
		
	var best = []
	var score = -INF
	
	for dial in all:
		if dial.constraints_count() == score:
			best.push_back(dial)
		if best.empty() or dial.constraints_count() > score:
			score = dial.constraints_count()
			best = [dial]
	var res = null
	
	if best.size() == 1:
		res = best[0]
	elif best.size() > 1:
		# si on a un ordre parmi les répliques
		# alors on les passes dans l'ordre
		var last_dial = GQuestTracker.last_dialog
		
		var random_index = randi() % best.size()
		res = best[random_index]
		
		if res != null:
			var p = self.prev(res)
			while p != null:
				res = p
				p = self.prev(res)

	GQuestTracker.last_dialog = res
	return res

func get_by_id(id):
	for dial in self.dialogs:
		if dial.id == id:
			return dial
	return null		
